import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Settings } from './settings';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable()
export class ArticleService {

  constructor(private http:HttpClient, private settings: Settings) { }

  getArticles() {
    let url = this.settings.API_URL + '/articles';
    return this.http.get(url);
  }
}
