import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import { AppFooterComponent } from './app-footer/app-footer.component';

import { ArticleService } from './article.service';
import { MenuService } from './menu.service';
import { Settings } from './settings';


@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    AppFooterComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpClientModule
  ],
  providers: [ArticleService, Settings, MenuService],
  bootstrap: [AppComponent]
})
export class AppModule { }
