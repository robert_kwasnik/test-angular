import { Component, OnInit } from '@angular/core';
import { ArticleService } from './article.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public allArticles;
  public articles;
  public featured;

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.getArticles();
  }

  getArticles() {
    return this.articleService.getArticles().subscribe(
      data => {
        this.allArticles = data['data'];
        this.featured = this.allArticles[0];
        this.articles = this.allArticles.filter((item, index) => index > 0);
      },
      err => console.log(err),
      () => console.log("Pobieranie zakończone!")
    );
  }

  title = 'Strona główna';
}
