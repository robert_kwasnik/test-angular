import { Component, OnInit } from '@angular/core';
import { MenuService } from '../menu.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './app-navbar.component.html',
  styleUrls: ['./app-navbar.component.scss']
})
export class AppNavbarComponent implements OnInit {
  public menu;

  constructor(private menuService: MenuService) { }

  ngOnInit() {
    this.getMenu();
  }

  getMenu() {
    return this.menuService.getMenu().subscribe(
      data => {
        this.menu = data['data'];
        console.log(this.menu);
      },
      err => console.log(err),
      () => console.log("Pobieranie zakończone!")
    );
  }

}
